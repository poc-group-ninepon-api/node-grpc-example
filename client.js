require('dotenv').config()
const grpc = require("grpc");
const fs = require('fs');
const express = require('express')
var randomWords = require('random-words');
const protoLoader = require("@grpc/proto-loader");
const packageDef = protoLoader.loadSync("todo.proto", {});
const grpcObject = grpc.loadPackageDefinition(packageDef);
const todoPackage = grpcObject.todoPackage;
const { body, validationResult } = require('express-validator');

const { header } = require("./client/helpers/clientSide")

const metadata = new grpc.Metadata();
// metadata.set('authorization', 'header-value');
const app = express()

// middleware
app.use((req, res, next) => {
    // Create a new gRPC client
    // const client = new todoService('localhost:50051', grpc.credentials.createInsecure());
    const grpcCredentials = grpc.credentials.createInsecure();
    const client = new todoPackage.Todo(
        `${process.env.SERVER_GRPC_HOST}:${process.env.SERVER_GRPC_PORT}`, 
        grpcCredentials
    )
    req.client = client;
    next();
});
app.use(express.json());

// write todo
app.post('/todo', 
    body('idcode').isAlphanumeric(),
    body('id').isNumeric(),
    body('text').not().isEmpty(),
    (req, res) => {
    // res.send('Hello World!')
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    req.client.createTodo( req.body, header(metadata, req.headers), (err, response) => {
        if (err) {
            res.status(500).json({ error: err });
        } else {
            console.log(`received from server ${JSON.stringify(response)}`);
            res.json(response);
        }
    });
})

// get todo
app.get('/todo', (req, res) => {
    req.client.readTodos({}, metadata, (err, response) => {
        if (err) {
            res.status(500).json({ error: err });
        } else {
            console.log(`received from server ${JSON.stringify(response)}`);
            res.json(response);
        }
    });
})

// client stream read
app.get('/todo/stream', (req, res) => {
    const call = req.client.readTodosStream();
    call.on("data", item => {
        console.log(`== readTodosStream on data ==`);
        console.log(JSON.stringify(item));
    }, header(metadata, req.headers))
    call.on("end", (e) =>{
        console.log(`readTodosStream... server done!`)
        res.status(204).json();
    })
})

// client stream write
app.post('/todo/client/stream',
    body('idcode').isAlphanumeric(),
    body('id').isNumeric(),
    body('text').not().isEmpty(),
    (req, res) => {
    // console.log(req.body);

    const call = req.client.createTodoClientStream();
    call.metadata = header(metadata, req.headers)
    
    setInterval(() => {
        call.write( { id: 2, idcode: 'aa12345', text: 'bbbb' } );
    }, 1000);
    
    // for(let i = 0 ; i < 20 ; i++){
    //     // stream receive from server in loop
    //     call.on("data", item => {
    //         // console.log(`== readTodosStream on data ==`);
    //         console.log(JSON.stringify(item));
    //     }, req.body, header(metadata, req.headers))
    // }
    
    call.on("end", e => { 
        console.log(`readTodosStream... server done!`)
    })

})

app.listen(process.env.CLIENT_EXPRESS_PORT, () => {
    console.log(`Client start via port: ${process.env.CLIENT_EXPRESS_PORT}`)
})