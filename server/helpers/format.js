const configApp = require("../configs/app")

function dataResponse(request_id){
    const res = {
        "request_id": request_id || configApp.serverName,
        "status": 204,
        "message": "success",
        "data": { }
    }

    return res
}

module.exports = {
    dataResponse
}