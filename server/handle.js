const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { authenticate } = require("./middlewares/authentication")
const { dataResponse } = require("./helpers/format")
var randomWords = require('random-words');

const todos = []
function createTodo(call, callback){
    const req = call.request
    authenticate(call, (err, result) => {
        if (err) {
            return callback(err);
        }
    });
    console.log(`+++++++++`);
    const todoItem = dataResponse()
    todoItem.data = {
        "idcode": req.idcode,
        "id": todos.length + 1,
        "text": req.text
    }

    todos.push( todoItem )
    callback(null, todoItem);
}

function createTodoClientStream(call, _){
    authenticate(call, (err, result) => {
        if (err) {
            return callback(err);
        }
    });
    //NOTE handle event data and end
    call.on('data', (response) => {
        console.log(response);
    });
    call.on('end', (e) => {
        console.log('Streaming ended');
    });
}

function createTodoServerStream(call, _){
    const req = call.request
    
    //NOTE handle event data and end
    call.on('data', (response) => {
        console.log(response);
    });
    call.on('end', (e) => {
        console.log('Streaming ended');
    });

    let i = 0
    const interval = setInterval(() => {
        const uuid = uuidv4()
        const todoItem = dataResponse()
        todoItem.data = {
            "idcode": `${uuid}`,
            "id":  `${req.id}`,
            "text":  `${req.text}`
        }
        call.write(todoItem); i++
        if(i === 30){
            clearInterval(interval)
            call.end();
        }
    }, 5000);
}

function readTodos(call, callback){
    const uuid = uuidv4()
    const todoItem = dataResponse()
    todoItem.data = {
        "idcode": `${uuid}`,
        "id":  0,
        "text":  `${randomWords()}`
    }
    callback( null, todoItem )
}

function readTodosStream(call, callback){
    let i = 0
    const interval = setInterval(() => {
        const uuid = uuidv4()
        const todoItem = dataResponse()
        todoItem.data = {
            "idcode": `${uuid}`,
            "id":  `${i}`,
            "text":  `${randomWords()}`
        }
        call.write(todoItem); i++
        if(i === 10){
            clearInterval(interval)
            call.end();
        }
    }, 1000);
}

function uploadFileStream(call, callback){
    let chunks = [];
    call.on('data', (chunk) => {
        chunks.push(chunk.data);
    });
    call.on('end', () => {
        let fileBuffer = Buffer.concat(chunks);
        let fileName = call.metadata.get('file_name')[0];
        fs.writeFile(`uploads/${fileName}`, fileBuffer, (err) => {
            if (err) {
                callback(err);
            } else {
                callback(null, { message: "File uploaded successfully." });
            }
        });
    });
}

module.exports = {
    createTodo,
    readTodos,
    createTodoClientStream,
    createTodoServerStream,
    readTodosStream,
    uploadFileStream
}