function header(metadata, headers){
    for (let key in headers) {
        metadata.set(key, headers[key]);
    }
    return metadata
}

module.exports = {
    header
}